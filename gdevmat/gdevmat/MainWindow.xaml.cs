﻿using gdevmat.Models;
using gdevmat.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace gdevmat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Initialization
        public MainWindow() // Void start
        {
            InitializeComponent();

        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);
            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        #endregion

        private Vector3 mousePosition = new Vector3();

        public Cube boat = new Cube()
        {
            Mass = 0.5f,
            Scale = new Vector3(5, 2, 5),
            Color = new Models.Color(1, 0.5, 0, 1)
        };

        public Circle fish = new Circle()
        {
            Color = new Models.Color(RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1)),
            Mass = RandomNumberGenerator.GenerateFloat(1, 3),
            radius = RandomNumberGenerator.GenerateFloat(1, 2),
            Position = new Vector3(RandomNumberGenerator.GenerateFloat(-40, 40), 30, 0)
        };

        private Liquid ocean = new Liquid(0, 0, 100, 50, 0.9f);

        private Vector3 gravity = new Vector3(0, -0.05f, 0);
        private Vector3 wind = new Vector3(-0.1f, 0, 0);

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "FINALS";
            OpenGL gl = args.OpenGL;

            // Clear The Screen And The Depth Buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

            // Move Left And Into The Screen
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            ocean.Render(gl);

            boat.Render(gl);
       
            fish.Render(gl);
            fish.ApplyForce(gravity);

            //Collision
            if (fish.Position.y <= -40)
            {
                fish.Position.y = -40;
                fish.Velocity.y *= -1;
            }
            if (fish.Position.x >= 55)
            {
                fish.Position.x = 55;
                fish.Velocity.x *= -1;
            }
            if (fish.Position.x <= -55)
            {
                fish.Position.x = -55;
                fish.Velocity.x *= -1;
            }

            if (ocean.Contains(fish))
            {
                var dragForce = ocean.CalculateDragForce(fish);
                fish.ApplyForce(dragForce);
                fish.ApplyForce(mousePosition);
            }
            if (fish.Position.x >= 5)
            {
                fish.ApplyForce(wind);
            }
           

            gl.LineWidth(5.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(3, 2);
            gl.Vertex(10, 9);
            gl.End();



            gl.LineWidth(1.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(10, 9);
            gl.Vertex((mousePosition.x * 30), (mousePosition.y * 30));
            gl.End();

        }

        #region Mouse Func
        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
            mousePosition.Normalize();
        }
        #endregion
    }
}
