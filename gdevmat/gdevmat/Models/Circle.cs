﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpGL;

namespace gdevmat.Models
{
    public class Circle : Moveable
    {
        const int TOTAL_CIRCLE_ANGLE = 360;

        public float radius = 1;
        
        public Circle()
        {
        }

        public Circle(Vector3 initPos)
        {
            this.Position = initPos;
        }

        public Circle(float x, float y, float z)
        {
            this.Position.x = x;
            this.Position.y = y;
            this.Position.z = z;
        }

        public override void Render(OpenGL gl)
        {
            base.Render(gl);
            gl.PointSize(2.0f);
            gl.Begin(OpenGL.GL_LINES);
            for (int i = 0; i <= TOTAL_CIRCLE_ANGLE; i++)
            {
                gl.Vertex(Position.x, Position.y);
                gl.Vertex(Math.Cos(i) * radius + Position.x, Math.Sin(i) * radius + Position.y);
            }
            gl.End();
        }
    }
}
