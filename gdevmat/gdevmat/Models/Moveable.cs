﻿using gdevmat.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat.Models
{
    public class Moveable
    {
        public Vector3 Position = new Vector3();
        public Vector3 Velocity = new Vector3();
        public Vector3 Acceleration = new Vector3();

        public Color Color = new Color();
        public float Mass = 1;

        public float G = 0.05f;


        public void ApplyGravity(float scalar = 0.1f)
        {
            this.Acceleration += (new Vector3(0, -scalar * Mass, 0) / Mass);
        }

        public void ApplyForce(Vector3 force)
        {
            // F = MA
            // A = F/M
            this.Acceleration += (force / Mass); //force accumulation
        }

        public virtual void Render(OpenGL gl)
        {
            gl.Color(Color.r, Color.g, Color.b, Color.a);
            UpdateMotion();
        }

        private void UpdateMotion()
        {
            this.Velocity += this.Acceleration;
            this.Position += this.Velocity;
            this.Acceleration *= 0;
        }

        public Vector3 CalculateAttraction(Moveable moveable)
        {
            var force = this.Position - moveable.Position;
            var distance = force.GetLength();

            distance = GdevmatUtils.Constrain(distance, 5, 25);

            force.Normalize();
            var strength = (this.G * this.Mass * moveable.Mass) / (distance * distance);
            force *= strength;
            return force;

        }
    }
}
